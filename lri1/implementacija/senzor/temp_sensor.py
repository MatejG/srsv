import argparse
import os
import random
import json
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from time import sleep

global current_temperature
global ac_status
global device_id


# po dokumentaciji mora biti u formatu: fcija(client,userdata,message)
def myCallback(client, userdata, message):
    print("Message Received !! \n")
    poruka = json.loads(message.payload)  # dictionary
    global ac_status
    global device_id

    if poruka.get("Id") == device_id:
        ac_status = poruka.get('Command')

    return


if __name__ == '__main__':

    global current_temperature
    global ac_status
    global device_id

    current_temperature=15
    ac_status="OFF"

    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--rootCA", action="store", required=True, dest="rootCAPath", help="Root CA file path")
    parser.add_argument("-c", "--cert", action="store", dest="certificatePath", help="Certificate file path")
    parser.add_argument("-k", "--key", action="store", dest="privateKeyPath", help="Private key file path")
    parser.add_argument("-id", "--deviceId", action="store", dest="deviceId", required=True,
                        help="Device id ")



    port = 8883
    end_point = "a3j16iw4kvp1kk-ats.iot.us-east-1.amazonaws.com"  # OK
    args = parser.parse_args()
    root_cert = args.rootCAPath
    certificatePath = args.certificatePath
    private_key = args.privateKeyPath
    device_id = args.deviceId

    # Inicijalizacija MQTT-a

    myAWSIoTMQTTClient = AWSIoTMQTTClient("sensor" + device_id)
    myAWSIoTMQTTClient.configureEndpoint(end_point, port)
    myAWSIoTMQTTClient.configureCredentials(root_cert, private_key, certificatePath)

    myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
    myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
    myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
    myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
    myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

    myAWSIoTMQTTClient.connect()
    print("Uspjesno spojeni\n")
    myAWSIoTMQTTClient.subscribe("command", 1, myCallback)

    sensor_data = {"Id": device_id,
                   "Temp_data": None}

    while True:

        if ac_status == "OFF":
            current_temperature += random.randint(-1, 1)
        elif ac_status == "Cooling":
            current_temperature -= random.randint(1, 3)
        elif ac_status == "Heating":
            current_temperature += random.randint(1, 3)
        else:
            print("Server commmand error\n")

        sensor_data["Temp_data"] = current_temperature

        try:
            myAWSIoTMQTTClient.publish("temp_sensor", json.dumps(sensor_data), 1)
        except:
            print("Publish unavailable\n")


        # ispis podataka
        os.system("clear")
        print("-" * 20, end='\n')
        print("Device Id : " + device_id, end="\n")
        print("Current Status : " + ac_status, end="\n")
        print("Current Temperature : " + str(current_temperature), end="\n")
        print("-" * 20, end='\n')
        sleep(15)
