import argparse

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from time import sleep
import json
import os

# po dokumentaciji mora biti u formatu: fcija(client,userdata,message)

global ac_status
global device_id


def myCallback(client, userdata, message):
    poruka = json.loads(message.payload)  # dictionary
    global ac_status
    global device_id
    print("Message Received !! \n")

    if poruka.get("Id") == device_id:
        ac_status = poruka.get('Command')

    return


if __name__ == '__main__':
    global ac_status
    global device_id
    ac_status="OFF"

    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--rootCA", action="store", required=True, dest="rootCAPath", help="Root CA file path")
    parser.add_argument("-c", "--cert", action="store", required=True, dest="certificatePath", help="Certificate file path")
    parser.add_argument("-k", "--key", action="store", required=True, dest="privateKeyPath", help="Private key file path")
    parser.add_argument("-id", "--deviceId", action="store", required=True, dest="deviceId",
                        help="Device id ")

    args = parser.parse_args()
    root_cert = args.rootCAPath
    certificatePath = args.certificatePath
    private_key = args.privateKeyPath
    device_id = args.deviceId

    port = 8883
    end_point = "a3j16iw4kvp1kk-ats.iot.us-east-1.amazonaws.com"  ## OK

    # Inicijalizacija MQTT-a
    myAWSIoTMQTTClient = AWSIoTMQTTClient("klima" + device_id)
    myAWSIoTMQTTClient.configureEndpoint(end_point, port)
    myAWSIoTMQTTClient.configureCredentials(root_cert, private_key, certificatePath)

    myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
    myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
    myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
    myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
    myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

    myAWSIoTMQTTClient.connect()
    print("Uspjesno spojeni\n")
    myAWSIoTMQTTClient.subscribe("command", 1, myCallback)



    while True:
        os.system("clear")
        print("-" * 20, end='\n')
        print("Device Id : " + device_id, end="\n")
        print("Current Status : " + ac_status, end="\n")
        print("-"*20 , end='\n')
        sleep(5)


