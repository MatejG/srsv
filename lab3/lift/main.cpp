#include <iostream>
#include <thread>
#include <mutex>
#include <cstdio>
#include <condition_variable>
#include <string.h>
#include <random>
#include <csignal>
#include "deque"
#include "unordered_map"
#include "vector"


#define NUMBER_OF_FLOORS 4
#define MAX_ON_FLOOR 10
#define MAX_IN_LIFT 6


using namespace std;


volatile bool interrupt = false;

//PROBLEM SE JAVLJA PRI DOLASKU DO ODREDISTA ZA POKUPIT PUTNIKA, ONDA SE ZAUSTAVI I NE IDE DALJE, NE APDEJTA SMJER KRETANJA

enum Prometnost {
    SLABO, NORMALNO, JAKO
};

enum Doors{ OPEN='O',CLOSED='Z'};

unordered_map<int, int> kat_to_pozicija =     //lookup tablica za prijevod kata u poziciju
        {
                {0, 0},
                {1, 2},
                {2, 4},
                {3, 6}
        };

unordered_map<int,int> pozicija_to_kat =   //lookup tablica za prijevod pozicije u kat
        {
                {0,0},
                {2,1},
                {4,2},
                {6,3}



        };


enum class Direction {
    GORE = 'G', DOLJE = 'D', STOJI = 'S'
};   //smjerovi kretanja, putnika i lifta

int period_cekanja=5;  //kolko ce se cesto stvarati novi putnici

int broj_kretanja_lifta = 0;   //koliko se lift mora kretati
int passengers_in_lift=0;
enum Doors lift_doors=CLOSED;

mutex m;


Direction smjer_lifta = Direction::STOJI;


struct putnik {
    char id;
    Direction smjer;
    int odrediste;
};


struct zahtjev {
    Direction smjer;
    int odrediste;
    int pocetak;

};


typedef struct zahtjev zahtjev;
typedef struct putnik putnik;


unordered_map<char, zahtjev> passenger_list;


//vektor putnika po katovima i brojac koliko ih ima po svakom katu
vector<putnik> entry_floors[NUMBER_OF_FLOORS];
vector<putnik> lift_space;
vector<putnik> exit_floors[NUMBER_OF_FLOORS];

deque<zahtjev> zahtjevi;   //red koji sadrzi sve zahtjeve prema liftu

int lift_position = 0;  //0 - 6 uracunati i medu katovi
int entry_num[NUMBER_OF_FLOORS] = {};

//sinkronizacija u slucaju da nema zahtjeva dok lift ceka



void signalHandler(int signum) {
    cout << "Zavrsavam program\n";
    interrupt = true;

    exit(signum);
}





void addToQueue(int pocetni, int odrediste, Direction smjer) {

    int temp_put;
    zahtjev novi_zahtjev;


    if (smjer == smjer_lifta) {     //isti smjer kretanja

        switch (smjer_lifta) {


            case Direction::GORE:

                if (kat_to_pozicija[pocetni] > lift_position) {    //zahtjev koji je na putu kretanja lifta

                    temp_put = abs(kat_to_pozicija[odrediste] - lift_position);
                    if (temp_put > broj_kretanja_lifta)
                        broj_kretanja_lifta = temp_put;

                } else {   //proso voz
                    novi_zahtjev.smjer = smjer;    //stavi novi zahtjev  na kraj reda zahtjeva
                    novi_zahtjev.odrediste = odrediste;
                    novi_zahtjev.pocetak = pocetni;
                    zahtjevi.push_back(novi_zahtjev);

                }
                break;


            case Direction::DOLJE :


                if (kat_to_pozicija[pocetni] < lift_position) {    //zahtjev koji je na putu kretanja lifta

                    temp_put = abs(kat_to_pozicija[odrediste] - lift_position);
                    if (temp_put > broj_kretanja_lifta)
                        broj_kretanja_lifta = temp_put;

                } else {   //proso voz
                    novi_zahtjev.smjer = smjer;    //stavi novi zahtjev  na kraj reda zahtjeva
                    novi_zahtjev.odrediste = odrediste;
                    novi_zahtjev.pocetak = pocetni;
                    zahtjevi.push_back(novi_zahtjev);

                }
                break;

        }


    } else {   //nece mu se otvoriti vrata dok lift ne odradi svoj smjer do kraja
        novi_zahtjev.smjer = smjer;    //stavi novi zahtjev  na kraj reda zahtjeva
        novi_zahtjev.odrediste = odrediste;
        novi_zahtjev.pocetak = pocetni;
        zahtjevi.push_back(novi_zahtjev);

    }
}


void lift() {

    zahtjev processing;
    int queue_size;

    while(true){

        if(interrupt)
            exit(0);




        switch (smjer_lifta) {




            case Direction::STOJI:    //lift nema smjer kretanja

              //  cout<<"ODREDIVANJE SMJERA\n\n";


               while(zahtjevi.empty()){}


                processing=zahtjevi.front();   //dohvati referencu prvog u redu


                if(kat_to_pozicija[processing.pocetak]<lift_position){    //ako je zahtjev ispod lifta
                    smjer_lifta=Direction::DOLJE;
                    broj_kretanja_lifta=lift_position-kat_to_pozicija[processing.pocetak];

                }else if(kat_to_pozicija[processing.pocetak]>lift_position){   //zahtjev je iznad lifta

                    smjer_lifta=Direction::GORE;
                  /*  if(processing.smjer==Direction::GORE)   //odstrani zahtjev ako je vec ide u tom smjeru
                        zahtjevi.pop_front(); */
                    broj_kretanja_lifta=kat_to_pozicija[processing.pocetak] - lift_position;
                }else{ //zahtjev se nalazi na katu na kojem je lift

                    smjer_lifta=processing.smjer;
                    broj_kretanja_lifta=abs(kat_to_pozicija[processing.odrediste] - lift_position);  //izracunaj pomak koji lift treba napraviti
                    zahtjevi.pop_front();
                }

                queue_size=zahtjevi.size();

                for (int i = 0; i < queue_size; ++i) {    //sortiraj nove zahtjeve
                    processing=zahtjevi.front();
                    zahtjevi.pop_front();   //odstrani element
                    m.lock();
                    addToQueue(processing.pocetak,processing.odrediste,processing.smjer);   //addToQueue ce ponovno sortirati red
                    m.unlock();

                }
                break;

            case Direction::DOLJE:    //lift ima odreden smjer
            case Direction::GORE:


                if (pozicija_to_kat.find(lift_position) != pozicija_to_kat.end()) {   //jesmo li trenutno na katu




                    auto it = lift_space.begin();
                    while (it != lift_space.end())     //IZBACI ekipu koja ide iz lifta
                    {
                        if (kat_to_pozicija[it->odrediste]==lift_position) {
                            lift_doors=OPEN;
                            exit_floors[pozicija_to_kat[lift_position]].push_back(*it);
                            passenger_list.erase(it->id);
                            it = lift_space.erase(it);
                            passengers_in_lift--;
                        }
                        else {
                            ++it;
                        }
                    }

                    it=entry_floors[pozicija_to_kat[lift_position]].begin();
                    while (it != entry_floors[pozicija_to_kat[lift_position]].end())     //UBACI ekipu u lift koja ceka
                    {
                        if (it->smjer==smjer_lifta) {
                          //  cout<<"ZIV SAM"<<endl;
                            lift_doors=OPEN;
                            if(passengers_in_lift==MAX_IN_LIFT){
                                m.lock();
                                addToQueue(pozicija_to_kat[lift_position],it->odrediste,it->smjer);   //ako je krcat lift stavi zahtjev ponovno na kraj reda
                                m.unlock();
                                it++;
                                continue;

                            }
                            lift_space.push_back(*it);
                            passengers_in_lift++;
                            it = entry_floors[pozicija_to_kat[lift_position]].erase(it);     //obrisi putnika iz ulaznog kata
                            entry_num[pozicija_to_kat[lift_position]]--;
                        }
                        else {
                            ++it;
                        }
                    }

                    if(lift_doors==OPEN){
                        this_thread::sleep_for(chrono::seconds(5));  //vrata otvorena 5 sekundi
                        lift_doors=CLOSED;
                    }

                } //end if
                if(broj_kretanja_lifta!=0){    //jesmo li jos uvijek na smjeru

                    if(smjer_lifta== Direction::GORE)
                        lift_position++;
                    else
                        lift_position--;
                    broj_kretanja_lifta--;
                    this_thread::sleep_for(chrono::seconds(2)); // prijelaz iz jedne pozicije u drugu traje 2 sekunde
                }else
                    smjer_lifta=Direction::STOJI;

                break;


        } //end switch
    }//end while

}



void ispis() {



    vector<string> init_array={
                           "               Lift1     \n",
                           "Smjer/vrata:    S Z     \n",                           //Na mjesto S i Z idu smjer i status vratiju [2][16] i [2] [18]
                           "======================  Izašli\n",
                           "3:          |          |        \n",
                           "  ==========|          |        \n",
                           "2:          |          |        \n",
                           "  ==========|          |        \n",
                           "1:          |          |        \n",                   //lift granice [x][14] i [x][23]
                           "  ==========|          |        \n",
                           "0:          |          |        \n",            //prvi ulaz kat na [9][4], izlaz na [9][25]
                           "================================\n",
                           "id | od | do\n"} ;     //tablica putnika

    vector<string> work_array;
    int j;
    int k;


    while(true){


        if(interrupt)
            exit(0);

        work_array=init_array;


        //ispis smjera i statusa vratiju
        work_array[1][16]= static_cast<char>(smjer_lifta);
        work_array[1][18] = static_cast<char>(lift_doors);

        for (int i = 0; i < 4; ++i) {   //ispisi putnike po katovima
            j=4;
            k=25;
            for(auto x: entry_floors[i]){
                work_array[9-2*i][j++]=x.id;
            }

            for(auto x: exit_floors[i]){
                work_array[9-2*i][k++]=x.id;
            }

        }

        //  ispis pozicije lifta

        work_array[9-lift_position][13]='[';  //lijeva
        work_array[9-lift_position][22]=']';   //desna
        k=14;
        for(auto x: lift_space){
            work_array[9-lift_position][k++]=x.id;
        }

        cout<<"Broj putnika u liftu "<< passengers_in_lift<<endl;
        cout<<"POZICIJA LIFTA: "<<lift_position<<endl<<"BROJ POKRETA: "<<broj_kretanja_lifta<<endl<<"SMJER: "<<(char) smjer_lifta<<endl;
       // cout<<"BROJ ZAHTJEVA: "<<zahtjevi.size()<<endl<<endl;
        for(const auto& x : work_array){
            cout<<x;
        }
        for (auto x: passenger_list) {
            // Do stuff
            cout << x.first <<"  | "<<x.second.pocetak<<"  | "<<x.second.odrediste<<endl;
        }
        cout<<endl;

        for (auto & exit_floor : exit_floors) {    //obrisi putnike koji su na izlazu iz sustava
            exit_floor.clear();
        }

        this_thread::sleep_for(chrono::seconds(2));   //cekaj 2 sekunde
        system("clear");






    }
}



int main(int argc, char *argv[]) {

    int poc_kat;
    int odr;
    putnik novi;
    zahtjev mock;


    if (argc == 1) {
        period_cekanja = 5;
    } else if (argc == 2) {

        switch (stoi(argv[1])) {

            case SLABO:
                period_cekanja = 20;
                break;
            case NORMALNO:
                period_cekanja = 10;
                break;

            case JAKO:
                period_cekanja = 5;
                break;

            default:
                period_cekanja = 10;

        }
    } else {
        cerr << "Neispravno koristenje, previse argumenata\n";
        exit(1);
    }
    cout << "Period cekanja: " << period_cekanja << " sekundi\n";


    signal(SIGINT, signalHandler);
    thread(lift).detach();  //glavna dretva
    thread(ispis).detach();  //dretva za ispis

    random_device rd;
    mt19937 rng(rd());
    uniform_int_distribution<int> uniFloor(0, 3); // kat na kojem ce se nalazit i na koji ce ic
    uniform_int_distribution<int> uniId(65, 90); //random id za putnika

    while (true) {

        this_thread::sleep_for(chrono::seconds(period_cekanja));
        poc_kat = uniFloor(rng);
        /*
        novi.id='k';
        novi.odrediste=2;
        novi.smjer=Direction::GORE;
        entry_floors[1].push_back(novi);
        addToQueue(1,2,Direction::GORE);
         */


        if (entry_floors[poc_kat].size() < MAX_ON_FLOOR) {  //provjera za broj ljudi po katu, ako nema onda nista ceka drugog putnika

            while ((odr = uniFloor(rng)) == poc_kat) {};  //rng dok ne dobije kat koji nije isti na kojem se nalazi

            if (odr > poc_kat)
                novi.smjer = Direction::GORE;
            else
                novi.smjer = Direction::DOLJE;
            novi.odrediste = odr;
            novi.id = (char) uniId(rng);
           // cout<<"Stvoren novi putnik id/odrediste/smjer/poc kat: "<<novi.id<<" "<<" "<<novi.odrediste<<" "<<(char)novi.smjer<<poc_kat<<endl<<endl;
            entry_floors[poc_kat].push_back(novi);   //dodaj novog putnika na kat
            mock.odrediste=odr;
            mock.pocetak=poc_kat;
            passenger_list[novi.id]=mock;
            entry_num[poc_kat]++;                   //inkrementiraj broj putnika na katu
            m.lock();
            addToQueue(poc_kat, odr, novi.smjer );               //pozovi funkciju za obradu novog zahtjeva
            m.unlock();

        }
        //break; //test
    }


}
