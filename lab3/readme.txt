
Program je strukturiran tako da se sastoji od 2 dretve: 

    -jedna koja stvara putnike
    -jedna koja glumi lift

Putnici su simulirani sa strukturama koje imaju svoj smjer kretanja, id i odredište. Početni kat nekog putnika određen je indeksom unutar polja vektora entry_floors koji glumi početne katove. 

Lift određuje smjerove kretanja pomoću reda zahtjeva zahtjevi. S funkcijom addToQueue se pojedini putnik stavlja na svoju poziciju unutar reda.
 Ako je smjer kretanja putnika isti kao i smjer kretanja lifta i u putanji lifta je, neće se ni dodavati u red nego će samo ažurirati broj pomaka lifta ako se lift treba pomaknuti veći broj pomaka nego što trenutna treba. Inače ako nije na putanji lifta s istim smjerom ili lift uopće nema smjer, zahtjev se stavlja na kraj reda čekanja.

Kada lift promjeni smjer svi zahtjevi se ponovno s AddtoQueue funkcijom stavljaju u red čekanja te im se promijeni prioritet 
    po potrebi.
