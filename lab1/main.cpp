#include <iostream>
#include <thread>
#include <mutex>
#include <cstdio>
#include <condition_variable>
#include <string.h>
#include <random>
#include <csignal>


#define MAX_DRETVI 20


volatile bool interrupt = false;
int broj_dretvi = 0;
int trenutno_stanje = 0;


using namespace std;
//   SZ SI JZ JI    ćošak na kojem se pješak može nalaziti
int pjesaci_lokacija[2][4] = {{0, 0, 0, 0},   // SJ    Redovi oznacavaju smjer kretanja
                              {0, 0, 0, 0}};  // IZ



                        //   S  J   I     Z     Strane na raskrižju
int auti_lokacija[2][4] = {{0, 0, NULL, NULL},    //SJ      Smjerovi kretanja
                           {NULL, NULL, 0, 0}};     //IZ


int auti_prijelaz[2][4]{{0, 0, NULL, NULL},     //analogno kao i auti_lokacija
                        {NULL, NULL, 0, 0}};


int pjesaci_prijelaz[2][4] = {{0, 0, 0, 0},      //analagno kao i pjesaci_lokacija
                              {0, 0, 0, 0}};


enum svijetlo {
    CRVENO, ZELENO
};

enum cosak {
    SZ, SI, JZ, JI
};

enum vrsta_semafora {
    CESTA, PJESACKI
};

enum smjer_kretanja {
    SJ, IZ
};

enum strana_na_raskrizju {
    S, J, I, Z
};

//CESTA  PJESACI
enum svijetlo semafori_UPR_komande[2][2] = {{CRVENO, CRVENO},   //SJ
                                            {CRVENO, CRVENO}};        //IZ

//CESTA  PJESACI
enum svijetlo semafori_zarulje[2][2] = {{CRVENO, CRVENO},   //SJ
                              {CRVENO, CRVENO}};        //IZ








mutex m;
condition_variable cv[2][2];    //analogni raspored kao i za globalnu varijablu semafor_zarulje[smjer][vrsta]



void signalHandler(int signum) {
    cout << "Zavrsavam program\n";
    interrupt = true;

    exit(signum);
}


void semafor(enum smjer_kretanja smjer, enum vrsta_semafora vrsta) {
    svijetlo trenutno_svijetlo = CRVENO;


    while (true) {


        if (interrupt) {
            cv[smjer][vrsta].notify_all();    //obavijesti dretve da je kraj programa
            exit(2);
        }


        if (trenutno_svijetlo != semafori_UPR_komande[smjer][vrsta]) {

         //   printf("Promjena stanja %d %d\n", smjer, vrsta);

            trenutno_svijetlo = static_cast<svijetlo>(semafori_UPR_komande[smjer][vrsta]);
            semafori_zarulje[smjer][vrsta] = semafori_UPR_komande[smjer][vrsta];
            if (trenutno_svijetlo == ZELENO)
                cv[smjer][vrsta].notify_all();    //obavijesti dretve sto cekaju o promjeni


        }


    }
}

void RAS() {

    //1. SLIKA KOJA SE NE MIJENJA
    char slika_pocetna[11][12] = {{' ', ' ', ' ', '|', ' ', '|', ' ', '|', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', '|', ' ', '|', ' ', '|', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', '|', ' ', '|', ' ', '|', ' ', ' ', ' ', '\n'},
                                  {'-', '-', '-', '+', ' ', '|', ' ', '+', '-', '-', '-', '\n'},
                                  {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '\n'},
                                  {'-', '-', '-', ' ', ' ', ' ', ' ', ' ', '-', '-', '-', '\n'},
                                  {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '\n'},
                                  {'-', '-', '-', '+', ' ', '|', ' ', '+', '-', '-', '-', '\n'},
                                  {' ', ' ', ' ', '|', ' ', '|', ' ', '|', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', '|', ' ', '|', ' ', '|', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', '|', ' ', '|', ' ', '|', ' ', ' ', ' ', '\n'},
    };
    char slika_trenutna[11][12];

    while (true) {



        if (interrupt)
            exit(2);

        memcpy(slika_trenutna, slika_pocetna, sizeof(slika_pocetna));   //prebrisi sliku

        cout<<"Trenutno stanje raskrizja: "<<trenutno_stanje<<endl;
        cout<<"Broj dretvi: "<<broj_dretvi<<endl<<endl;

        printf("STATUS PJESAKA:\n");
        printf(" SZ|SI|JZ|JI --> strane na raskrizju\n");

        for (int i = 0; i < 2; ++i) {
            cout<<" ";
            for (int j = 0; j < 4; ++j) {
                printf("%02d|", pjesaci_lokacija[i][j]);

            }
            if(i==0)
                cout<<" SJ --> smjerovi kretanja\n";
            else
                cout<<" IZ\n";
        }

        cout<<endl;

        cout<<"STATUS AUTOMOBILA:\n";
        printf(" Sjever: %d\n Jug: %d\n Istok: %d\n Zapad: %d\n", auti_lokacija[0][0],auti_lokacija[0][1],auti_lokacija[1][2],auti_lokacija[1][3]);
        cout<<endl;




/*
        cout<<"SVIJETLA SEMAFORA\n";
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                cout<<(svijetlo)semafori_zarulje[i][j]<<" ";

            }
            cout<<endl;
        }
        cout<<endl;
        */


        //2. PRIJELAZI SUDIONIKA U PROMETU

        if (pjesaci_prijelaz[IZ][SZ] || pjesaci_prijelaz[IZ][SI]) {    //gornji dio
            slika_trenutna[1][4] = 'p';
            slika_trenutna[1][6] = 'p';
        }
        if (pjesaci_prijelaz[IZ][JZ] || pjesaci_prijelaz[IZ][JI]) {      //donji
            slika_trenutna[9][4] = 'p';
            slika_trenutna[9][6] = 'p';
        }

        if (pjesaci_prijelaz[SJ][SZ] || pjesaci_prijelaz[SJ][JZ]) {   //lijevo
            slika_trenutna[4][1] = 'p';
            slika_trenutna[6][1] = 'p';
        }

        if (pjesaci_prijelaz[SJ][SI] || pjesaci_prijelaz[SJ][JI]) {    //desno
            slika_trenutna[4][9] = 'p';
            slika_trenutna[6][9] = 'p';
        }


        if (auti_prijelaz[SJ][S]) {
            for (auto &i : slika_trenutna) {
                i[4] = 'A';
            }
        }
        if (auti_prijelaz[SJ][J]) {
            for (auto &i : slika_trenutna) {
                i[6] = 'A';
            }
        }

        if (auti_prijelaz[IZ][I]) {
            for (int i = 0; i < 11; ++i) {
                slika_trenutna[4][i] = 'A';
            }
        }

        if (auti_prijelaz[IZ][Z]) {
            for (int i = 0; i < 11; ++i) {
                slika_trenutna[6][i] = 'A';
            }
        }

        //3. SUDIONICI U PROMETU KOJI CEKAJU

        if (pjesaci_lokacija[IZ][SZ] > 0)
            slika_trenutna[1][2] = 'p';
        if (pjesaci_lokacija[IZ][SI] > 0)
            slika_trenutna[1][8] = 'p';
        if (pjesaci_lokacija[IZ][JZ] > 0)
            slika_trenutna[9][2] = 'p';
        if (pjesaci_lokacija[IZ][JI] > 0)
            slika_trenutna[9][8] = 'p';

        if (pjesaci_lokacija[SJ][SZ] > 0)
            slika_trenutna[2][1] = 'p';
        if (pjesaci_lokacija[SJ][JZ] > 0)
            slika_trenutna[8][1] = 'p';
        if (pjesaci_lokacija[SJ][SI] > 0)
            slika_trenutna[2][9] = 'p';
        if (pjesaci_lokacija[SJ][JI] > 0)
            slika_trenutna[8][9] = 'p';


        if (auti_lokacija[SJ][S] > 0)
            slika_trenutna[0][4] = 'A';
        if (auti_lokacija[SJ][J] > 0)
            slika_trenutna[10][6] = 'A';
        if (auti_lokacija[IZ][Z] > 0)
            slika_trenutna[6][0] = 'A';
        if (auti_lokacija[IZ][I] > 0)
            slika_trenutna[4][10] = 'A';



        //4. ISPIS SLIKE


        for (auto & i : slika_trenutna) {
            for (char j : i) {
                cout<<j;
            }
        }

        cout<<"\n\n\n";

        this_thread::sleep_for(chrono::seconds(2));

    }


}


void pjesak(enum smjer_kretanja smjer, enum cosak kantun) {


    m.lock();
    pjesaci_lokacija[smjer][kantun]++;
    m.unlock();
    if (interrupt)
        exit(2);

    unique_lock<mutex> ul(m);
    if(semafori_zarulje[smjer][PJESACKI]!=ZELENO){

        cv[smjer][PJESACKI].wait(ul, [smjer] { return semafori_zarulje[smjer][PJESACKI] == ZELENO; });  //ceka na zeleno
    }



    pjesaci_prijelaz[smjer][kantun] = 1;
    ul.unlock();
    if (interrupt)
        exit(2);

    this_thread::sleep_for(chrono::seconds(10));    //prelazi cestu
    m.lock();
    pjesaci_lokacija[smjer][kantun]--;   //napusti sustav
    pjesaci_prijelaz[smjer][kantun] = 0;   //zavrsi prijelaz
    broj_dretvi--;
    m.unlock();

}

void automobil(enum smjer_kretanja smjer, enum strana_na_raskrizju strana) {
    m.lock();
    auti_lokacija[smjer][strana]++;
    m.unlock();

    if (interrupt)
        exit(2);
    unique_lock<mutex> ul(m);
    if(semafori_zarulje[smjer][CESTA]!=ZELENO) {
        cv[smjer][CESTA].wait(ul, [smjer] { return semafori_zarulje[smjer][CESTA] == ZELENO; });  //ceka na
    }

    auti_prijelaz[smjer][strana] = 1;
    ul.unlock();

    if (interrupt)
        exit(2);


    this_thread::sleep_for(chrono::seconds(5));    //prelazi cestu krace treba jer je auto
    m.lock();
    auti_lokacija[smjer][strana]--;   //napusti sustav
    auti_prijelaz[smjer][strana] = 0;
    broj_dretvi--;
    m.unlock();


}

void UPR() {


    int suma_pjesaka;

    int upravljacka_sekvenca[6][2][2] = {{{CRVENO, CRVENO},
                                                 {CRVENO, CRVENO}},  //0
                                         {{CRVENO, CRVENO},
                                                 {ZELENO, ZELENO}},  //1
                                         {{CRVENO, CRVENO},
                                                 {ZELENO, CRVENO}}, //2
                                         {{CRVENO, CRVENO},
                                                 {CRVENO, CRVENO}}, //3
                                         {{ZELENO, ZELENO},
                                                 {CRVENO, CRVENO}}, //4
                                         {{ZELENO, CRVENO},
                                                 {CRVENO, CRVENO}} //5
    };

    while (true) {


        if (interrupt)
            exit(2);

        switch (trenutno_stanje) {

            case 0:
                this_thread::sleep_for(chrono::seconds(5));
                break;

            case 1:
                suma_pjesaka = 0;
                for (auto &num : pjesaci_lokacija[IZ])
                    suma_pjesaka += num;

                if (suma_pjesaka > 0)
                    this_thread::sleep_for(chrono::seconds(30));
                else
                    this_thread::sleep_for(chrono::seconds(15));
                break;

            case 2:
                this_thread::sleep_for(chrono::seconds(10));
                break;

            case 3:
                this_thread::sleep_for(chrono::seconds(5));
                break;

            case 4:
                suma_pjesaka = 0;
                for (auto &num : pjesaci_lokacija[SJ])
                    suma_pjesaka += num;

                if (suma_pjesaka > 0)
                    this_thread::sleep_for(chrono::seconds(30));
                else
                    this_thread::sleep_for(chrono::seconds(15));
                break;

            case 5:
                this_thread::sleep_for(chrono::seconds(10));
                break;

            default:
                cout << "Greska u upravljackom racunalu\n";


        }


        trenutno_stanje = (trenutno_stanje + 1) % 6;
        memcpy(semafori_UPR_komande, upravljacka_sekvenca[trenutno_stanje], sizeof(semafori_UPR_komande));



    }

};


int main() {

    signal(SIGINT, signalHandler);
    int broj_pjesaka = 0;
    int broj_autiju = 0;

    thread(semafor, SJ, CESTA).detach();
    thread(semafor, SJ, PJESACKI).detach();
    thread(semafor, IZ, CESTA).detach();
    thread(semafor, IZ, PJESACKI).detach();


    thread(RAS).detach();
    thread(UPR).detach();



    random_device rd;
    mt19937 rng(rd());
    uniform_int_distribution<int> uniTime(10, 20); // pjesaci i auti dolaze svakih 10 -20s
    uniform_int_distribution<int> uniSmjer(0, 1); // smjer kretanja  //tj. 50 50
    uniform_int_distribution<int> uniPjesakKantun(0, 3); // kantun na kojem se nalazi

    smjer_kretanja auto_smjer;


    while (true) {


        if (broj_dretvi == MAX_DRETVI) {
            cout<<"Previse dretvi";
            this_thread::sleep_for(chrono::seconds(5));
        }


        thread(pjesak, (enum smjer_kretanja) uniSmjer(rng), (enum cosak) uniPjesakKantun(rng)).detach();
        m.lock();
        broj_dretvi++;
        m.unlock();

        this_thread::sleep_for(chrono::seconds(uniTime(rng)));

        if (broj_dretvi == 10) {
            this_thread::sleep_for(chrono::seconds(5));
        }


        auto_smjer = (smjer_kretanja) uniSmjer(rng);

        switch (auto_smjer) {

            case SJ:

                thread(automobil, auto_smjer,
                       (enum strana_na_raskrizju) uniSmjer(rng)).detach();   //generiraj S ili J za lokaciju

                break;

            case IZ:

                thread(automobil, auto_smjer,
                       (enum strana_na_raskrizju) (uniSmjer(rng) + 2)).detach();   //generiraj I ili Z za lokaciju

                break;

        }
        m.lock();
        broj_dretvi++;
        m.unlock();


    }


}
