Izvorni kod je sadrzan u datoteci main.cpp i prevodi se na Unix okruzenju sa :

g++ main.cpp -std=c++0x -pthread -o raskrizje

, gdje je raskrizje izvrsna datoteka koja se pokrece sa ./raskrizje .
Program svakih 10-20 sekundi stvara novog sudionika u prometu na nasumicoj lokaciji s nasumicnim smjerom kretanja. Ispis se sastoji od grafickog prikaza raskrizja,
tablicnog prikaza gdje se koji sudionik nalazi i kamo ide, te konacno ispis u kojem se stanju raskrizje nalazi.


Stanja raskrizja su sljedeca:

-Stanje 0 sve crveno nitko se ne krece
-Stanje 1 smjer I-Z prohodan za aute i pjesake
-Stanje 2 smjer I-Z prohodan za aute
-Stanje 3 sve crveno nitko se ne krece 
-Stanje 4 smjer S-J prohodan za aute i pjesake
-Stanje 5 smjer S-J prohodan za aute

Poslije stanja 5 sustav ide u stanje 0.

