#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <mqueue.h>
#include <string.h>

#define PRIO_GENERATOR 50
#define BILLION 1E9
#define MAX_PORUKA_U_REDU 10
#define MAX_VELICINA_PORUKE 100

const pthread_mutex_t monitor = PTHREAD_MUTEX_INITIALIZER;

struct segment_id { // segment zajednickog spremnika
    pthread_mutex_t m; // monitor za osiguravanje jedinstvenosti identifikatora
    long id; // zadnji dodijeljeni identifikator posla
};

struct opisnik_posla {
    long id;
    long trajanje;
    char* naziv_spremnika;
};

int J, K;
int ids;
struct segment_id *seg_id;
char* naziv_seg_id;
char* naziv_reda;

static void posao_generator(long id) {

    char* s = getenv("SRSV_LAB5"); // dohvat varijable okoline
    naziv_seg_id = malloc(strlen(s)+2);
    strcpy(naziv_seg_id, "/");
    strcat(naziv_seg_id, s);

    // stvori segment (ili se povezi na nj)
    ids = shm_open (naziv_seg_id, O_CREAT | O_RDWR, 00600);
    if (ids == -1 || ftruncate ( ids, sizeof(struct segment_id) ) == -1) {
        exit(1);
    }
    seg_id = mmap ( NULL, sizeof(struct segment_id), PROT_READ | PROT_WRITE, MAP_SHARED, ids, 0 );

    if (seg_id == (void*) -1){
        exit(1);
    }
    close(ids);

    // zakljucaj i uzmi iducih J brojeva te otkljucaj i odspoji se od zaj. spremnika
    // ako ne postoji segment u /dev/shm onda ga inicijaliziraj
    char* datoteka = malloc(strlen(s)+10);
    strcpy(datoteka, "/dev/shm/");
    strcat(datoteka, s);

    if(access(datoteka, F_OK) == -1) {
        struct segment_id pocetno = {
                .m = PTHREAD_MUTEX_INITIALIZER,
                .id = 0
        };
        seg_id = &pocetno;
    }
    free(datoteka);

    int min, max;    //zauzmi J identifikatora
    pthread_mutex_lock (&(seg_id->m));
    min = seg_id->id + 1; // 1
    max = min + J - 1; // J=3
    seg_id->id = max;
    pthread_mutex_unlock (&(seg_id->m));

    // sad stvori novih J poslova
    printf("\n");
    for(long i=0; i<J; i++) {
        long id_posla = min;
        min++;
        long trajanje = rand()%(K-1+1)+1;

        // sad stvori zaj. spremnik (ime je "/" + SRSV_LAB5 + "-" + <ID-posla>)
        long *seg_pod;
        char* naziv_seg_pod;
        int idsp;


        naziv_seg_pod = malloc(strlen(s)+5); // npr. /lab5sim-1
        strcpy(naziv_seg_pod, "/");
        strcat(naziv_seg_pod, s);
        strcat(naziv_seg_pod, "-");
        char str[21];
        sprintf(str, "%ld", id_posla);
        strcat(naziv_seg_pod, str);
        //printf("ime:%s\n", naziv_seg_pod);

        idsp = shm_open (naziv_seg_pod, O_CREAT | O_RDWR, 00600);
        if (idsp == -1 || ftruncate ( idsp, trajanje * sizeof(long) ) == -1)
            exit(1);
        seg_pod = mmap ( NULL, trajanje * sizeof(long), PROT_READ | PROT_WRITE, MAP_SHARED, idsp, 0 );

        if (seg_pod == (void*) -1)
            exit(1);
        close(idsp);

        // podatak je jedan slucajan broj za svaku jedinicu vremena posla
        for(long t=0; t<trajanje; t++)
            seg_pod[t] = rand()%(999-100+1)+100; // sluc broj 100-999

        // sad napravi opisnik posla
        struct opisnik_posla opisnik = {
                .id = id_posla,
                .trajanje = trajanje,
                .naziv_spremnika = naziv_seg_pod
        };

        // ispis u konzolu
        printf("G: posao %ld %ld %s [ ", id_posla, trajanje, naziv_seg_pod);
        for(int i=0; i<trajanje; i++)
            printf("%ld ", seg_pod[i]);
        printf("]\n");

        // posalji opisnik u red poruka (ime je "/" + SRSV_LAB5, nakon slanja sleep(1))
        char poruka[MAX_VELICINA_PORUKE];
        struct mq_attr attr;
        sprintf(poruka, "%ld %ld %s", opisnik.id, opisnik.trajanje, opisnik.naziv_spremnika);
        //printf("poruka:%s\n", poruka);

        mqd_t opisnik_reda;
        size_t duljina = strlen (poruka) + 1;
        unsigned prioritet = 10;

        attr.mq_flags = 0;
        attr.mq_maxmsg = MAX_PORUKA_U_REDU;
        attr.mq_msgsize = MAX_VELICINA_PORUKE;
        opisnik_reda = mq_open (naziv_seg_id, O_WRONLY | O_CREAT, 00600, &attr);
        if ( opisnik_reda == (mqd_t) -1 ) {
            perror ( "proizvodjac:mq_open" );
            exit(1);
        }
        if ( mq_send ( opisnik_reda, poruka, duljina, prioritet ) ) {
            perror ( "mq_send" );
            exit(1);
        }
        sleep(1);

        // odspoji se od spremnika
        munmap (seg_pod, trajanje*sizeof(long));
    }

    // odspajanje od zajednickog spremnika za identifikator
    munmap (seg_id, sizeof(struct segment_id));
}

static void *generator(void *param) {
    posao_generator((long) param);
    return NULL;
}

// stvori red poruka i zajednicki spremnik

int main(int argc, char *argv[]) {

    asm volatile("" ::: "memory");
    srand((unsigned)time(NULL));

    // ucitaj elemente naredbenog retka
    if(argc < 3) {
        printf(">>> Premali broj argumenata naredbenog retka\n");
        return 0;
    }

    J = atoi(argv[1]); // broj poslova
    K = atoi(argv[2]); // koji traju maksimalno K jedinica vremena

    pthread_t id;
    pthread_attr_t attr;
    struct sched_param prioritet;
    long nacin_rasp = SCHED_RR;

    // postavljanje nacina rasporedjivanja
    pthread_attr_init(&attr);
    pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attr, nacin_rasp);

    // stvaranje jedne dretve "generator"
    prioritet.sched_priority = PRIO_GENERATOR;
    pthread_attr_setschedparam(&attr, &prioritet);
    if ( pthread_create ( &id, &attr, generator, (void *) (0) ) ) { // ovo je nulta dretva
        printf(">>> Greska prilikom stvaranja dretve\n");
        return 1;
    }

    free(naziv_seg_id);
    pthread_join (id, NULL);

    return 0;
}
