#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

#define MAX_VELICINA_PORUKE 100
 
struct my_struct
{
  char num[MAX_VELICINA_PORUKE];
  struct my_struct* next;
};
 
 
struct my_list
{
  struct my_struct* head;
  struct my_struct* tail;
  int size;
};
 
 
struct my_list* list_add_element( struct my_list*, const char*);
char* list_remove_element( struct my_list*);
int velicina_liste(struct my_list*);
 
 
struct my_list* list_new(void);
struct my_list* list_free( struct my_list* );
 
void list_print( const struct my_list* );
void list_print_element(const struct my_struct* );
 
 
/* Will always return the pointer to my_list */
struct my_list* list_add_element(struct my_list* s, const char* i)
{
  struct my_struct* p = malloc( 1 * sizeof(*p) );
 
  if( NULL == p )
    {
      fprintf(stderr, "IN %s, %s: malloc() failed\n", __FILE__, "list_add");
      return s; 
    }
 
  strcpy(p->num, i);  
  //p->num = i;
  p->next = NULL;
 
 
  if( NULL == s )
    {
      printf("Queue not initialized\n");
      free(p);
      return s;
    }
  else if( NULL == s->head && NULL == s->tail )
    {
      /* printf("Empty list, adding p->num: %d\n\n", p->num);  */
      s->head = s->tail = p;
      s->size++;
      return s;
    }
  else if( NULL == s->head || NULL == s->tail )
    {
      fprintf(stderr, "There is something seriously wrong with your assignment of head/tail to the list\n");
      free(p);
      return NULL;
    }
  else
    {
      /* printf("List not empty, adding element to tail\n"); */
      s->tail->next = p;
      s->tail = p;
      s->size++;
    }
 
  return s;
}
 
 
/* This is a queue and it is FIFO, so we will always remove the first element */
char* list_remove_element( struct my_list* s )
{
  struct my_struct* h = NULL;
  struct my_struct* p = NULL;
 
  h = s->head;
  char *num = (char *) malloc(sizeof(char) * MAX_VELICINA_PORUKE);
  strcpy(num, h->num);
  p = h->next;
  free(h);
  s->head = p;
  s->size--;
  if( NULL == s->head )  s->tail = s->head;   /* The element tail was pointing to is free(), so we need an update */
 
  return num;
}
   
 
/* ---------------------- small helper fucntions ---------------------------------- */
struct my_list* list_free( struct my_list* s )
{
  while( s->head )
    {
      list_remove_element(s);
    }
 
  return s;
}
 
struct my_list* list_new(void)
{
  struct my_list* p = malloc( 1 * sizeof(*p));
  if( NULL == p )
    {
      fprintf(stderr, "LINE: %d, malloc() failed\n", __LINE__);
    }
 
  p->head = p->tail = NULL;
  p->size=0;
  return p;
}
 
 
void list_print( const struct my_list* ps )
{
  struct my_struct* p = NULL;
 
  if( ps )
    {
      for( p = ps->head; p; p = p->next )
    {
      list_print_element(p);
    }
    }
 
  printf("------------------\n");
}
 
 
void list_print_element(const struct my_struct* p )
{
  if( p ) 
    {
      printf("Num = %s\n", p->num);
    }
  else
    {
      printf("Can not print NULL struct \n");
    }
}

int velicina_liste(struct my_list* p) {
    return p->size;    
}

