#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <mqueue.h>
#include <string.h>
#include <signal.h>
#include "list.h"


#define BILLION 1E9
#define PRIO_RADNA 40
#define PRIO_ZAPRIMA 60
#define MAX_VELICINA_PORUKE 100

int N, M;

int SHUTDOWN=0;

struct my_list* mt = NULL; // lokalni FIFO za poruke

pthread_t *rid;


void kraj_programa(){


    printf("Završavam s radom ./posluzitelj %d %d \n", N, M);

    for (int i = 0; i < N; ++i) {
        int status = pthread_kill(rid[i], SIGKILL);   //ubij radne dretve
        if (status < 0)
            perror("pthread_kill failed");
    }
    system("bash clean.sh");  //obrisi shm i mqueue direktorij od podataka da se ne gomilaju


    SHUTDOWN=1;
}



void potraj_sekundu() {
	for(unsigned long long int i=0; i<283535085; i++)
		printf("");
	return;
}

static void probudi_se(void) {}   //funkcija za budenje radne dretve

static void obrada_signala1(long id) {
	// ako ima poslova u lokalnom spremniku, uzmi prvi i obradi ga

	char* poruka = (char *) malloc(sizeof(char) * MAX_VELICINA_PORUKE);
	long id_posla, t;  //trajanje
	char ime[MAX_VELICINA_PORUKE];
    int idsp;

	//ima poruka u lokalnom redu
	if(velicina_liste(mt)>0) {
		poruka = list_remove_element(mt);  //skini poruku i procitaj sadrzaj
		//printf("poruka=%s\n", poruka);
		sscanf(poruka, "%ld %ld %s", &id_posla, &t, ime);
		long broj_podataka_u_seg = t;    //1 podatak po sekundi

        //printf("Ime segmenta : %s\n", ime);
    	// izvuci podatke iz segmenta naziva "ime"

		idsp = shm_open (ime, O_CREAT | O_RDWR, 00600);   //prima pokaznik na memorijski segment

        //ako je neispravan pokaznik ili ne moze se podesiti velicina, javi gresku
		if (idsp == -1 || ftruncate ( idsp, broj_podataka_u_seg*sizeof(long) ) == -1) {
			perror("Greska shm_open\n");
			exit(1);
		}
		long *seg_pod;
		seg_pod = mmap ( NULL, broj_podataka_u_seg*sizeof(long), PROT_READ | PROT_WRITE, MAP_SHARED, idsp, 0 );

		// radno cekanje
		for(long i=0; i<broj_podataka_u_seg; i++){
			printf("R%ld : id:%ld obrada podatka: %ld (%ld/%ld)\n",
					id, id_posla, seg_pod[i], i+1, broj_podataka_u_seg);
			potraj_sekundu();
		}
		close(idsp);   //zatvori pokaznik
		munmap (seg_pod, broj_podataka_u_seg*sizeof(long));       //oslobodi se iz segmenta
	}
	else
		printf("R%ld : nema poslova, spavam\n", id);
}

static void posao_radna(long id) {

	struct sigaction act;
	sigemptyset ( &act.sa_mask );
	act.sa_flags = 0;
	act.sa_handler = &probudi_se;     //koristena je prazna funkcija kako bi se prenesao argument id
	sigaction ( SIGUSR1, &act, NULL );   //na SIGUSR1 pocni radit

	while(1) {
		pause();
		obrada_signala1(id);
	}
}

static void posao_zaprima(long id) {

	char* naziv_reda;
	char* s = getenv("SRSV_LAB5"); // dohvat varijable okoline za naziv reda poruka
	naziv_reda = malloc(strlen(s)+2);
	strcpy(naziv_reda, "/");
	strcat(naziv_reda, s);


    //Definiraj funkciju obrade za sigterm
    struct sigaction act;
    sigemptyset ( &act.sa_mask );
    act.sa_flags = 0;
    act.sa_handler = &kraj_programa;     //zakljucaj na kraj_programa funkciju
    sigaction ( SIGTERM, &act, NULL );   //SIGTERM


	mt = list_new();        //napravi lokalni red poruka

	mqd_t opisnik_reda;
	char poruka[MAX_VELICINA_PORUKE];
	char stara_poruka[MAX_VELICINA_PORUKE];
	size_t duljina;
	unsigned prioritet;
	opisnik_reda = mq_open (naziv_reda, O_RDONLY);
	if ( opisnik_reda == (mqd_t) -1 ) {
		perror ( "potrosac:mq_open" );
		exit(1);
	}

	struct timespec tm;
	int vrijeme_od_zadnje_poruke = 0;

	while(1) {


	    if(SHUTDOWN){    //zaprimljen je signal za kraj rada, zavrsi sa radom

	        return ;

	    }
		// ako u redu poruka mq ima poruka, onda ih dohvati, cekaj 30 sek.
		clock_gettime(CLOCK_REALTIME, &tm);
		tm.tv_sec += 30; // 30  sekundi provjeravaj ima li poruka u redu poruka
		duljina = mq_timedreceive(opisnik_reda, poruka, MAX_VELICINA_PORUKE, &prioritet, &tm);
		//printf("nakon timed\n");

		if (strcmp(stara_poruka, poruka) == 0) {  //ako je poruka identicna , znaci da nije dosla nova poruka
			// nije dosla poruka u 30 sekundi
			vrijeme_od_zadnje_poruke = 30;
		}
		else {

			long id, t;
			char ime[MAX_VELICINA_PORUKE];

			strcpy(stara_poruka, poruka);
			sscanf(poruka, "%ld %ld %s", &id, &t, ime);

			printf("P: zaprimio %ld %ld %s\n", id, t, ime);

			// stavi u lista_poruka
			list_add_element(mt, poruka);
		}
		// ako u listi ima barem N poslova koji ukupno trebaju minimalno M jedinica vremena
		// onda signaliziraj sve radne dretve (probudi ih)

		int ukupno_trajanje = 0; //ukupno trajanje poslova u lokalnom spremniku
   		for(int i=0; i<velicina_liste(mt); i++){
			long id, t;
			char ime[MAX_VELICINA_PORUKE];

			struct my_struct* p = NULL;
			if(mt) {
				for(p = mt->head; p; p = p->next) {
					sscanf(p->num, "%ld %ld %s", &id, &t, ime);
					ukupno_trajanje += t;
				}
			}
		}

		if(velicina_liste(mt) >= N && ukupno_trajanje >= M){
			printf("P: ima posla, budim radnike\n");
			// signaliziraj radne dretve
			for(long i=0; i<N; i++){
				int status = pthread_kill(rid[i], SIGUSR1);    //javi dretvama da se pokrenu sa signalom
				if (status < 0)
					perror("pthread_kill failed");
			}
		}
		else if(vrijeme_od_zadnje_poruke >= 30 && velicina_liste(mt) >= 1){
			vrijeme_od_zadnje_poruke = 0;
			printf("P: pokrecem zaostale poslove (nakon isteka vise od 30 sekundi)\n");
			// signaliziraj radne dretve
			for(long i=0; i<N; i++){
				int status = pthread_kill(rid[i], SIGUSR1);   //javi ostalim dretvama da se pokrenu sa signalom
				if (status < 0)
					perror("pthread_kill failed");
			}
		}
	}
}

static void *radna(void *param) {
	posao_radna((long) param);
	return NULL;
}

static void *zaprima(void *param) {
    posao_zaprima((long) param);
    return NULL;
}
int main(int argc, char *argv[]) {

	asm volatile("" ::: "memory");

	// ucitaj elemente naredbenog retka
	if(argc < 3) {
		printf(">>> Premali broj argumenata naredbenog retka\n");
		return 0;
	}


    N = atoi(argv[1]); // broj radnih dretvi
    M = atoi(argv[2]); // koje traju minimalno M jedinica vremena



	rid = malloc(N*sizeof(pthread_t));     //alociraj memorije za pokazivac na radne dretve

	pthread_t zid;     //identifikator za dretvu koja zaprima

    pthread_attr_t attr_zaprima;
    struct sched_param prioritet_zaprima;

    pthread_attr_t attr_radna;
    struct sched_param prioritet_radna;

    long nacin_rasp = SCHED_RR;

	// postavljanje nacina rasporedjivanja zaprima
    pthread_attr_init(&attr_zaprima);
    pthread_attr_setinheritsched(&attr_zaprima, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attr_zaprima, nacin_rasp);

    // postavljanje nacina rasporedjivanja radna
    pthread_attr_init(&attr_radna);
    pthread_attr_setinheritsched(&attr_radna, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attr_radna, nacin_rasp);

    //postavi prioritete na dretve
    prioritet_zaprima.sched_priority = PRIO_ZAPRIMA;
    pthread_attr_setschedparam(&attr_zaprima, &prioritet_zaprima);
    prioritet_radna.sched_priority = PRIO_RADNA;
    pthread_attr_setschedparam(&attr_radna, &prioritet_radna);

	// stvaranje jedne dretve "zaprima"
	if ( pthread_create ( &zid, &attr_zaprima, zaprima, (void *) (0) ) ) {
		printf(">>> Greska prilikom stvaranja dretve zaprimatelja\n");
		return 1;
	}

	// stvaranje N radnih dretvi koje trebaju minimalno M jedinica vremena
	for(long i=0;i<N;i++){
		// void* i+1 je argument - redni broj dretve
		if ( pthread_create ( &rid[i], &attr_radna, radna, (void *) (i+1) ) ) {
			printf(">>> Greska prilikom stvaranja radne dretve\n");
			return 1;
		}
	}

	for (long i = 0; i < N; i++)
		pthread_join (rid[i], NULL);

	pthread_join (zid, NULL);

	return 0;
}

