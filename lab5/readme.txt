

Administratorske ovlasti:

u terminalu upisati: "su -" pa svoju sudo šifru.



Namjestanje varijable okoline:


U admin terminalu upisati: "export SRSV_LAB5=lab5sim"


Prevođenje programa:

gcc posluzitelj.c -o posluzitelj.out -pthread -Wall -lrt
gcc generator.c -o generator.out -pthread -Wall -lrt

Lokacije zajedničkih spremnika i redova poruka:

/dev/mqueue -  redovi poruka
/dev/shm -  zajednicki spremnici

Ugasi posluzitelja :

kill -15 pid_posluzitelja
