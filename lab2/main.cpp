#include <iostream>
#include <thread>
#include <mutex>
#include <cstdio>
#include <condition_variable>
#include <string.h>
#include <random>
#include <csignal>


#define MAX_DRETVI 20


volatile bool interrupt = false;
int broj_dretvi = 0;
int trenutno_stanje = 0;


using namespace std;
//   SZ SI JZ JI    ćošak na kojem se pješak može nalaziti
int pjesaci_lokacija[2][4] = {{0, 0, 0, 0},   // SJ    Redovi oznacavaju smjer kretanja
                              {0, 0, 0, 0}};  // IZ



                        //   S  J   I     Z     Strane na raskrižju
int auti_lokacija[3][4] = {{0, 0, 0, 0},    //RAVNO      Smjerovi kretanja
                           {0, 0, 0, 0},    //DESNO
                           {0, 0, 0, 0}};   //LIJEVO


int auti_prijelaz[3][4]={{0, 0, 0, 0},    //RAVNO      Smjerovi kretanja, analogno kao auti_lokacija
                         {0, 0, 0, 0},    //DESNO
                         {0, 0, 0, 0}};   //LIJEVO


int pjesaci_prijelaz[2][4] = {{0, 0, 0, 0},      //analagno kao i pjesaci_lokacija
                              {0, 0, 0, 0}};


enum svijetlo {
    CRVENO, ZELENO, Na    //Na znaci da nije implementiran taj semafor
};

enum cosak {
    SZ, SI, JZ, JI
};

enum vrsta_semafora {
    CESTA, PJESACKI
};

enum smjer_kretanja {
    SJ, IZ, DESNO_SEM,LIJEVO_SJ,LIJEVO_IZ
};

enum auto_smjer {
    RAVNO,DESNO,LIJEVO
};

enum strana_na_raskrizju {
    S, J, I, Z
};

//CESTA  PJESACI
enum svijetlo semafori_UPR_komande[5][2] = {{CRVENO, CRVENO},   //SJ
                                            {CRVENO, CRVENO},   //IZ
                                            {CRVENO, Na},    //DESNO_SEM
                                            {CRVENO,Na},     //LIJEVO_SJ
                                            {CRVENO,Na}};    //LIJEVO_IZ



                                        //CESTA  PJESACI
enum svijetlo semafori_zarulje[5][2] = {{CRVENO, CRVENO},   //SJ
                                        {CRVENO, CRVENO},   //IZ
                                        {CRVENO,Na},    //DESNO_SEM
                                        {CRVENO,Na},    //LIJEVO_SJ
                                        {CRVENO,Na} };   //LIJEVO_IZ








mutex m;
condition_variable cv[5][2];    //analogni raspored kao i za globalnu varijablu semafor_zarulje[smjer][vrsta]



void signalHandler(int signum) {
    cout << "Zavrsavam program\n";
    interrupt = true;

    exit(signum);
}


void semafor(int smjer, enum vrsta_semafora vrsta) {
    svijetlo trenutno_svijetlo = CRVENO;


    while (true) {


        if (interrupt) {
            cv[smjer][vrsta].notify_all();    //obavijesti dretve da je kraj programa
            exit(2);
        }


        if (trenutno_svijetlo != semafori_UPR_komande[smjer][vrsta]) {

         //   printf("Promjena stanja %d %d\n", smjer, vrsta);

            trenutno_svijetlo = static_cast<svijetlo>(semafori_UPR_komande[smjer][vrsta]);
            semafori_zarulje[smjer][vrsta] = semafori_UPR_komande[smjer][vrsta];
            if (trenutno_svijetlo == ZELENO)
                cv[smjer][vrsta].notify_all();    //obavijesti dretve sto cekaju o promjeni
        }


    }
}

void RAS() {    

    //1. SLIKA KOJA SE NE MIJENJA
    char slika_pocetna[15][16] = {{' ', ' ', ' ', '|', ' ',' ',' ', '|', ' ',' ',' ', '|', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', '|', ' ',' ',' ', '|', ' ',' ',' ', '|', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', '|', ' ',' ',' ', '|', ' ',' ',' ', '|', ' ', ' ', ' ', '\n'},
                                  {'-', '-', '-', '+', ' ',' ',' ', ' ', ' ',' ',' ', '+', '-', '-', '-', '\n'},
                                  {' ', ' ', ' ', ' ', ' ',' ',' ', ' ', ' ',' ',' ', ' ', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', ' ', ' ',' ',' ', ' ', ' ',' ',' ', ' ', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', ' ', ' ',' ',' ', ' ', ' ',' ',' ', ' ', ' ', ' ', ' ', '\n'},
                                  {'-', '-', '-', ' ', ' ',' ',' ', ' ', ' ',' ',' ', ' ', '-', '-', '-', '\n'},
                                  {' ', ' ', ' ', ' ', ' ',' ',' ', ' ', ' ',' ',' ', ' ', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', ' ', ' ',' ',' ', ' ', ' ',' ',' ', ' ', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', ' ', ' ',' ',' ', ' ', ' ',' ',' ', ' ', ' ', ' ', ' ', '\n'},
                                  {'-', '-', '-', '+', ' ',' ',' ', ' ', ' ',' ',' ', '+', '-', '-', '-', '\n'},
                                  {' ', ' ', ' ', '|', ' ',' ',' ', '|', ' ',' ',' ', '|', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', '|', ' ',' ',' ', '|', ' ',' ',' ', '|', ' ', ' ', ' ', '\n'},
                                  {' ', ' ', ' ', '|', ' ',' ',' ', '|', ' ',' ',' ', '|', ' ', ' ', ' ', '\n'}

                                  };
    char slika_trenutna[15][16];

    while (true) {



        if (interrupt)
            exit(2);

        memcpy(slika_trenutna, slika_pocetna, sizeof(slika_pocetna));   //prebrisi sliku

        cout<<"Trenutno stanje raskrizja: "<<trenutno_stanje<<endl;
        cout<<"Broj dretvi: "<<broj_dretvi<<endl<<endl;

        printf("STATUS PJESAKA:\n");
        printf(" SZ|SI|JZ|JI --> strane na raskrizju\n");

        for (int i = 0; i < 2; ++i) {
            cout<<" ";
            for (int j = 0; j < 4; ++j) {
                printf("%02d|", pjesaci_lokacija[i][j]);

            }
            if(i==0)
                cout<<" SJ --> smjerovi kretanja\n";
            else
                cout<<" IZ\n";
        }

        cout<<endl;
        /*
        cout<<"STATUS AUTOMOBILA:\n";
        printf(" Sjever: %d\n Jug: %d\n Istok: %d\n Zapad: %d\n", auti_lokacija[0][0],auti_lokacija[0][1],auti_lokacija[1][2],auti_lokacija[1][3]);
        cout<<endl;
         */




/*
        cout<<"SVIJETLA SEMAFORA\n";
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                cout<<(svijetlo)semafori_zarulje[i][j]<<" ";

            }
            cout<<endl;
        }
        cout<<endl;
        */


        //2. PRIJELAZI SUDIONIKA U PROMETU

        if (pjesaci_prijelaz[IZ][SZ] || pjesaci_prijelaz[IZ][SI]) {    //gornji dio
            for (int i = 4; i <=10; ++i) {
                if(i==7)
                    continue;
                slika_trenutna[1][i]='p';

                
            }
        }
        if (pjesaci_prijelaz[IZ][JZ] || pjesaci_prijelaz[IZ][JI]) {      //donji
            for (int i = 4; i <=10; ++i) {
                if(i==7)
                    continue;
                slika_trenutna[13][i]='p';


            }
        }

        if (pjesaci_prijelaz[SJ][SZ] || pjesaci_prijelaz[SJ][JZ]) {   //lijevo
            for (int i = 4; i <=10; ++i) {
                if(i==7)
                    continue;
                slika_trenutna[i][1]='p';


            }
        }

        if (pjesaci_prijelaz[SJ][SI] || pjesaci_prijelaz[SJ][JI]) {    //desno
            for (int i = 4; i <=10; ++i) {
                if(i==7)
                    continue;
                slika_trenutna[i][13]='p';


            }
        }


        if (auti_prijelaz[SJ][S]) {
            for (auto &i : slika_trenutna) {
                i[5] = 'R';
            }
        }
        if (auti_prijelaz[SJ][J]) {
            for (auto &i : slika_trenutna) {
                i[9] = 'R';
            }
        }

        if (auti_prijelaz[IZ][I]) {
            for (int i = 0; i < 15; ++i) {
                slika_trenutna[5][i] = 'R';
            }
        }

        if (auti_prijelaz[IZ][Z]) {
            for (int i = 0; i < 15; ++i) {
                slika_trenutna[9][i] = 'R';
            }
        }

        if(auti_prijelaz[DESNO][S]){
            for (int i = 0; i < 5; ++i) {
                slika_trenutna[i][4]='D';
            }
            for (int i = 0; i <4 ; ++i) {
                slika_trenutna[4][i]='D';

            }

        }
        if(auti_prijelaz[LIJEVO][S]){

            for (int i = 0; i < 4; ++i) {
                slika_trenutna[i][6]='L';

            }

            for (int i = 7; i <12 ; ++i) {
                slika_trenutna[i-3][i]='L';
            }

            for (int i = 12; i < 15; ++i) {
                slika_trenutna[8][i]='L';

            }
        }

        if(auti_prijelaz[DESNO][J]){

            for (int i = 14; i > 9; i--) {
                slika_trenutna[i][10]='D';
            }
            for (int i = 11; i <15 ; ++i) {
                slika_trenutna[10][i]='D';

            }



        }


        if(auti_prijelaz[LIJEVO][J]){

            for (int i = 14; i > 10; i--) {
                slika_trenutna[i][8]='L';
            }

            slika_trenutna[10][7]='L';
            slika_trenutna[9][6]='L';
            slika_trenutna[8][5]='L';
            slika_trenutna[7][4]='L';


            for (int i = 0; i < 4; ++i) {
                slika_trenutna[6][i]='L';

            }
        }


        if(auti_prijelaz[DESNO][I]){

            for (int i = 0; i < 4; ++i) {
                slika_trenutna[i][10]='D';
            }
            for (int i = 11; i < 15; ++i) {
                slika_trenutna[4][i]='D';
            }

        }

        if(auti_prijelaz[LIJEVO][I]){

            for (int i = 14; i > 9; i--) {
                slika_trenutna[i][6]='L';
            }

            slika_trenutna[10][7]='L';
            slika_trenutna[9][8]='L';
            slika_trenutna[8][9]='L';
            slika_trenutna[7][10]='L';



            for (int i = 11; i < 15; ++i) {
                slika_trenutna[6][i]='L';

            }
        }

        if(auti_prijelaz[DESNO][Z]){

            for (int i = 0; i < 5; ++i) {
                slika_trenutna[10][i]='D';
            }
            for (int i = 11; i < 15; ++i) {
                slika_trenutna[i][4]='D';
            }

        }

        if(auti_prijelaz[LIJEVO][Z]){

            for (int i = 0; i < 4; ++i) {
                slika_trenutna[8][i]='L';
            }

            slika_trenutna[7][4]='L';
            slika_trenutna[6][5]='L';
            slika_trenutna[5][6]='L';
            slika_trenutna[4][7]='L';


            for (int i = 0; i < 4; ++i) {
                slika_trenutna[i][8]='L';
            }

        }






        //3. SUDIONICI U PROMETU KOJI CEKAJU

        if (pjesaci_lokacija[IZ][SZ] > 0)
            slika_trenutna[1][2] = 'p';
        if (pjesaci_lokacija[IZ][SI] > 0)
            slika_trenutna[1][12] = 'p';
        if (pjesaci_lokacija[IZ][JZ] > 0)
            slika_trenutna[13][2] = 'p';
        if (pjesaci_lokacija[IZ][JI] > 0)
            slika_trenutna[13][12] = 'p';

        if (pjesaci_lokacija[SJ][SZ] > 0)
            slika_trenutna[2][1] = 'p';
        if (pjesaci_lokacija[SJ][JZ] > 0)
            slika_trenutna[12][1] = 'p';
        if (pjesaci_lokacija[SJ][SI] > 0)
            slika_trenutna[2][13] = 'p';
        if (pjesaci_lokacija[SJ][JI] > 0)
            slika_trenutna[12][13] = 'p';


        if (auti_lokacija[SJ][S] > 0)
            slika_trenutna[0][5] = 'R';
        if (auti_lokacija[DESNO][S] > 0)
            slika_trenutna[0][4] = 'D';
        if (auti_lokacija[LIJEVO][S] > 0)
            slika_trenutna[0][6] = 'L';

        if (auti_lokacija[SJ][J] > 0)
            slika_trenutna[14][9] = 'R';
        if (auti_lokacija[DESNO][J] > 0)
            slika_trenutna[14][10] = 'D';
        if (auti_lokacija[LIJEVO][J] > 0)
            slika_trenutna[14][8] = 'L';

        if (auti_lokacija[IZ][Z] > 0)
            slika_trenutna[9][0] = 'R';
        if (auti_lokacija[DESNO][Z] > 0)
            slika_trenutna[10][0] = 'D';
        if (auti_lokacija[LIJEVO][Z] > 0)
            slika_trenutna[8][0] = 'L';

        if (auti_lokacija[IZ][I] > 0)
            slika_trenutna[5][14] = 'R';
        if (auti_lokacija[DESNO][I] > 0)
            slika_trenutna[4][14] = 'D';
        if (auti_lokacija[LIJEVO][I] > 0)
            slika_trenutna[6][14] = 'L';



        //4. ISPIS SLIKE


        for (auto & i : slika_trenutna) {
            for (char j : i) {
                cout<<j;
            }
        }

        cout<<"\n\n\n";

        this_thread::sleep_for(chrono::seconds(2));

    }


}


void pjesak(enum smjer_kretanja smjer, enum cosak kantun) {


    m.lock();
    pjesaci_lokacija[smjer][kantun]++;
    m.unlock();
    if (interrupt)
        exit(2);

    unique_lock<mutex> ul(m);
    if(semafori_zarulje[smjer][PJESACKI]!=ZELENO){

        cv[smjer][PJESACKI].wait(ul, [smjer] { return semafori_zarulje[smjer][PJESACKI] == ZELENO; });  //ceka na zeleno
    }



    pjesaci_prijelaz[smjer][kantun] = 1;
    ul.unlock();
    if (interrupt)
        exit(2);

    this_thread::sleep_for(chrono::seconds(10));    //prelazi cestu
    m.lock();
    pjesaci_lokacija[smjer][kantun]--;   //napusti sustav
    pjesaci_prijelaz[smjer][kantun] = 0;   //zavrsi prijelaz
    broj_dretvi--;
    m.unlock();

}

void automobil(enum auto_smjer smjer, enum strana_na_raskrizju strana) {
    int smjer_poravnat;
    m.lock();
    auti_lokacija[smjer][strana]++;
    m.unlock();


    if(strana==S || strana==J){

        switch(smjer){

            case RAVNO:
                smjer_poravnat=SJ;
                break;
            case DESNO:
                smjer_poravnat=DESNO_SEM;
                break;
            case LIJEVO:
                smjer_poravnat=LIJEVO_SJ;
                break;

        }


    } else{
        switch(smjer){

            case RAVNO:
                smjer_poravnat=IZ ;
                break;
            case DESNO:
                smjer_poravnat=DESNO_SEM;
                break;
            case LIJEVO:
                smjer_poravnat=LIJEVO_IZ;
                break;

        }



    }





    if (interrupt)
        exit(2);
    unique_lock<mutex> ul(m);
    if(semafori_zarulje[smjer_poravnat][CESTA]!=ZELENO) {
        cv[smjer_poravnat][CESTA].wait(ul, [smjer_poravnat] { return semafori_zarulje[smjer_poravnat][CESTA] == ZELENO; });  //ceka na
    }

    auti_prijelaz[smjer][strana] = 1;
    ul.unlock();

    if (interrupt)
        exit(2);


    this_thread::sleep_for(chrono::seconds(5));    //prelazi cestu krace treba jer je auto
    m.lock();
    auti_lokacija[smjer][strana]--;   //napusti sustav
    auti_prijelaz[smjer][strana] = 0;
    broj_dretvi--;
    m.unlock();


}

void UPR() {


    int suma_pjesaka;

    int upravljacka_sekvenca[11][5][2] = { {{CRVENO, CRVENO},
                                            {CRVENO, CRVENO},
                                            {CRVENO, Na},
                                            {CRVENO,Na},
                                            {CRVENO,Na}},   //0.
                                           {{ZELENO, CRVENO},
                                                   {CRVENO, CRVENO},
                                                   {ZELENO, Na},
                                                   {CRVENO,Na},
                                                   {CRVENO,Na}},   //1
                                           {{ZELENO, ZELENO},
                                                   {CRVENO, CRVENO},
                                                   {CRVENO, Na},
                                                   {CRVENO,Na},
                                                   {CRVENO,Na}},   //2
                                           {{ZELENO, CRVENO},
                                                   {CRVENO, CRVENO},
                                                   {CRVENO, Na},
                                                   {CRVENO,Na},
                                                   {CRVENO,Na}},   //3
                                           {{CRVENO, CRVENO},
                                                   {CRVENO, CRVENO},
                                                   {CRVENO, Na},
                                                   {CRVENO,Na},
                                                   {CRVENO,Na}},  //4
                                           {{CRVENO, CRVENO},
                                                   {ZELENO, CRVENO},
                                                   {ZELENO, Na},
                                                   {CRVENO,Na},
                                                   {CRVENO,Na}},  //5
                                           {{CRVENO, CRVENO},
                                                   {ZELENO, ZELENO},
                                                   {CRVENO, Na},
                                                   {CRVENO,Na},
                                                   {CRVENO,Na}},  //6
                                           {{CRVENO, CRVENO},
                                                   {ZELENO, CRVENO},
                                                   {CRVENO, Na},
                                                   {CRVENO,Na},
                                                   {CRVENO,Na}},  //7
                                           {{CRVENO, CRVENO},
                                                   {CRVENO, CRVENO},
                                                   {CRVENO, Na},
                                                   {CRVENO,Na},
                                                   {CRVENO,Na}},  //8
                                           {{CRVENO, CRVENO},
                                                   {CRVENO, CRVENO},
                                                   {ZELENO, Na},
                                                   {ZELENO,Na},
                                                   {CRVENO,Na}},  //9
                                           {{CRVENO, CRVENO},
                                                   {CRVENO, CRVENO},
                                                   {ZELENO, Na},
                                                   {CRVENO,Na},
                                                   {ZELENO,Na}}



    };

    while (true) {


        if (interrupt)
            exit(2);

        switch (trenutno_stanje) {

            case 0:
                this_thread::sleep_for(chrono::seconds(5));
                break;

            case 1:
                this_thread::sleep_for(chrono::seconds(15));

                break;

            case 2:
                suma_pjesaka = 0;
                for (auto &num : pjesaci_lokacija[SJ])
                    suma_pjesaka += num;

                if (suma_pjesaka > 0)
                    this_thread::sleep_for(chrono::seconds(30));
                else
                    this_thread::sleep_for(chrono::seconds(15));
                break;

            case 3:
                this_thread::sleep_for(chrono::seconds(10));
                break;

            case 4:
                this_thread::sleep_for(chrono::seconds(5));

                break;

            case 5:
                this_thread::sleep_for(chrono::seconds(15));
                break;

            case 6:
                suma_pjesaka = 0;
                for (auto &num : pjesaci_lokacija[IZ])
                    suma_pjesaka += num;

                if (suma_pjesaka > 0)
                    this_thread::sleep_for(chrono::seconds(30));
                else
                    this_thread::sleep_for(chrono::seconds(15));
                break;
            case 7:
                this_thread::sleep_for(chrono::seconds(10));
                break;
            case 8:
                this_thread::sleep_for(chrono::seconds(5));
                break;
            case 9:
                this_thread::sleep_for(chrono::seconds(10));
                break;
            case 10:
                this_thread::sleep_for(chrono::seconds(10));
                break;


            default:
                cout << "Greska u upravljackom racunalu\n";


        }


        trenutno_stanje = (trenutno_stanje + 1) % 11;
        memcpy(semafori_UPR_komande, upravljacka_sekvenca[trenutno_stanje], sizeof(semafori_UPR_komande));



    }

};


int main() {

    signal(SIGINT, signalHandler);
    int broj_pjesaka = 0;
    int broj_autiju = 0;

    thread(semafor, SJ, CESTA).detach();
    thread(semafor, SJ, PJESACKI).detach();
    thread(semafor, IZ, CESTA).detach();
    thread(semafor, IZ, PJESACKI).detach();
    thread(semafor,DESNO_SEM,CESTA).detach();
    thread(semafor,LIJEVO_SJ,CESTA).detach();
    thread(semafor,LIJEVO_IZ,CESTA).detach();


    thread(RAS).detach();
    thread(UPR).detach();



    random_device rd;
    mt19937 rng(rd());
    uniform_int_distribution<int> uniTime(10, 20); // pjesaci i auti dolaze svakih 10 -20s
    uniform_int_distribution<int> uniSmjer(0, 1); // smjer kretanja  //tj. 50 50
    uniform_int_distribution<int> uniSmjerAuto(0, 2); // smjer kretanja auta

    uniform_int_distribution<int> uniPjesakKantun(0, 3); // kantun na kojem se nalazi

    auto_smjer smjerAuto;


    while (true) {


        if (broj_dretvi == MAX_DRETVI) {
            cout<<"Previse dretvi";
            this_thread::sleep_for(chrono::seconds(5));
        }


        thread(pjesak, (enum smjer_kretanja) uniSmjer(rng), (enum cosak) uniPjesakKantun(rng)).detach();
        m.lock();
        broj_dretvi++;
        m.unlock();

        this_thread::sleep_for(chrono::seconds(uniTime(rng)));

        if (broj_dretvi == 10) {
            this_thread::sleep_for(chrono::seconds(5));
        }


        smjerAuto = (auto_smjer) uniSmjerAuto(rng);
        thread(automobil, smjerAuto, (strana_na_raskrizju)uniPjesakKantun(rng)).detach();

        m.lock();
        broj_dretvi++;
        m.unlock();


    }


}
