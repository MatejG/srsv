Izvorni kod je sadrzan u datoteci main.cpp i prevodi se na Unix okruzenju sa :

g++ main.cpp -std=c++0x -pthread -o raskrizje

, gdje je raskrizje izvrsna datoteka koja se pokrece sa ./raskrizje .
Program svakih 10-20 sekundi stvara novog sudionika u prometu na nasumicoj lokaciji s nasumicnim smjerom kretanja. Ispis se sastoji od grafickog prikaza raskrizja,
tablicnog prikaza gdje se koji sudionik nalazi i kamo ide, te konacno ispis u kojem se stanju raskrizje nalazi.


Stanja raskrizja su sljedeca:

-Stanje 0 sve crveno nitko se ne krece
-Stanje 1 smjer S-J i sva desna skretanja prohodna za aute 
-Stanje 2 smjer S-J prohodan za aute i pjesake
-Stanje 3 smjer S-J prohodan za aute 
-Stanje 4 sve crveno nitko se ne krece
-Stanje 5 smjer I-Z i sva desna skretanja prohodna za aute
-Stanje 6 smjer I-Z prohodan za aute i pjesake
-Stanje 7 smjer I-Z prohodan za aute 
-Stanje 8 sve crveno nitko se ne krece
-Stanje 9 sva desna i lijeva skretanja iz smjerova S i J su prohodni
-Stanje 10 sva desna i lijeva skretanja iz smjerova I i Z su prohodni

Poslije stanja 10 sustav ide u stanje 0.

